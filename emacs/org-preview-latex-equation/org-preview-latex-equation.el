(defun show-equation-posframe (process signal)
  (let ((inhibit-message t))
    (when (memq (process-status process) '(exit signal))
      (progn
	(kill-buffer (process-buffer process))
	(with-current-buffer (get-buffer-create "*latex-equation-preview*")
	  (posframe-hide "*latex-equation-preview")
	  (setq inhibit-read-only t)
	  (erase-buffer)
	  (insert-file-contents "~/.emacs.d/org-preview-latex-equation/equation.png")
	  (image-mode))
	(with-current-buffer (current-buffer) 
	  (when (posframe-workable-p)
	    (posframe-show "*latex-equation-preview*"
			   :position (point))))))))
;; (shell-command-sentinel process signal)))

(defun render-equation (latex)
  (let (equationfile previous new cmd img)
    (setq equationfile "~/.emacs.d/org-preview-latex-equation/equation.tex")
    (setq new (concat "\\\ documentclass{standalone}\n \\\ nofiles\n\\\ begin{document}\n" latex "\n\\\ end{document}"))
    (setq cmd (concat "/usr/bin/latex -interaction batchmode -output-directory ~/.emacs.d/org-preview-latex-equation " equationfile " > /dev/null && /usr/bin/dvipng -z 1 -q -T tight -D 200 " (file-name-sans-extension equationfile) ".dvi -o " (file-name-sans-extension equationfile) ".png > /dev/null"))
    (if (file-exists-p equationfile)
	(with-temp-buffer
	  (insert-file-contents equationfile)
	  (setq previous (nth 3 (split-string (buffer-string) "\n" t)))
	  (if (not (string= previous value))
	      (progn
		(write-region new nil equationfile)
		(save-window-excursion
		  (let* ((output-buffer (generate-new-buffer "*latex-equation-process*"))
			 (proc (progn
				 (async-shell-command cmd output-buffer)
				 (get-buffer-process output-buffer))))
		    (if (process-live-p proc)
			(set-process-sentinel proc #'show-equation-posframe)
		      (message "No process running.")))))
	    (with-current-buffer (current-buffer)
	      (posframe-show "*latex-equation-preview*"
			     :position beg))))
      (progn
	(write-region new nil equationfile)
	(save-window-excursion
	  (let* ((output-buffer (generate-new-buffer "*latex-equation-process*"))
		 (proc (progn
			 (async-shell-command cmd output-buffer)
			 (get-buffer-process output-buffer))))
	    (if (process-live-p proc)
		(set-process-sentinel proc #'show-equation-posframe)
	      (message "No process running."))))))))

(defun get-math-at-point ()
  "coucou"
  (if (org-inside-LaTeX-fragment-p)
      (let (beg end content)
	(let ((datum (org-element-context)))
	  (when (memq (org-element-type datum)
		      '(latex-environment latex-fragment))
	    (setq beg (org-element-property :begin datum))
	    (setq end (org-element-property :end datum))
	    (setq value (buffer-substring-no-properties beg end))
	    (render-equation value))))
    (posframe-hide-all)))

(defun latex-equation-preview-update ()
  (let ((inhibit-message t))
    (get-math-at-point)))

(defun delete-posframe ()
  (posframe-delete-all))

(define-minor-mode org-preview-latex-equation-mode
  "Preview latex equation in posframe"
  nil nil nil
  (if org-preview-latex-equation-mode
      (progn
	(add-hook 'post-command-hook #'latex-equation-preview-update nil t)
	(add-hook 'window-configuration-change-hook #'delete-posframe nil t))
    (progn
      (remove-hook 'post-command-hook #'latex-equation-preview-update t)
      (remove-hook 'window-configuration-change-hook #'delete-posframe t))))

(provide 'org-preview-latex-equation)
