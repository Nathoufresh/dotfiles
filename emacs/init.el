(require 'package)
;; (setq history-add-new-input nil)
(let* ((no-ssl (and (memq system-type '(windows-nt ms-dos))

                    (not (gnutls-available-p))))

       (proto (if no-ssl "http" "https")))

  (add-to-list 'package-archives (cons "melpa" (concat proto "://melpa.org/packages/")) t)

  (when (< emacs-major-version 24)

    (add-to-list 'package-archives (cons "gnu" (concat proto "://elpa.gnu.org/packages/")))))

(package-initialize)

(defun install-use-package ()
  (condition-case nil
      (require 'use-package)
    (file-error (progn
                  (package-refresh-contents)
                  (package-install 'use-package)
                  (funcall 'install-use-package)))))


(install-use-package)
(setq use-package-always-ensure t)

(use-package lsp-mode
  :init
  (setq lsp-enable-imenu t)
  (setq lsp-enable-links t)
  (setq lsp-use-native-json t)
  (setq lsp-enable-snippet nil)
  (setq lsp-enable-indentation nil)
  (setq lsp-ui-doc-enable nil)
  (setq lsp-idle-delay 0.3)
  (setq gc-cons-threshold 100000000)
  (setq lsp-log-io t)
  (setq read-process-output-max (* 1024 1024))
  :config
  (setq lsp-diagnostics-provider :flymake)
  :hook
  (js-mode . lsp)
  (python-mode . lsp)
  (c++-mode . lsp)
  (c-mode . lsp)
  :commands lsp)

(use-package lsp-ui
  :config
  (setq lsp-ui-peek-enable t)
  :bind
  (:map evil-normal-state-map
	("C-." . lsp-ui-imenu))
  ("C-." . lsp-ui-imenu)
  ("M-?" . lsp-ui-peek-find-definitions)
  :commands lsp-ui-mode)

;; Theming
(use-package all-the-icons)
(use-package emojify
  :hook
  (mu4e-view-mode . emojify-mode)
  (mu4e-headers-mode . emojify-mode))

;; (setq nathoufresh/light-theme-package 'modus-operandi-theme)
;; (setq nathoufresh/light-theme 'modus-operandi)
;; (setq nathoufresh/dark-theme-package 'modus-vivendi-theme)
;; (setq nathoufresh/dark-theme 'modus-vivendi)
(setq nathoufresh/light-theme-package 'doom-themes)
(setq nathoufresh/light-theme 'doom-one-light)
(setq nathoufresh/dark-theme-package 'doom-themes)
(setq nathoufresh/dark-theme 'doom-vibrant)

(defun toggle-theme ()
  (interactive)
  (if (called-interactively-p)
      (if (string-equal (car custom-enabled-themes) nathoufresh/dark-theme)
	  (funcall 'nathoufresh/set-light-theme)
	(funcall 'nathoufresh/set-dark-theme))
    (let ((hour (string-to-number (format-time-string "%H"))))
      (if (and (>= hour 8) (<= hour 20))
	  (funcall 'nathoufresh/set-light-theme)
	(funcall 'nathoufresh/set-dark-theme)))))

(defun nathoufresh/set-dark-theme ()
  (condition-case nil
      (progn
	(require nathoufresh/dark-theme-package)
	(disable-theme nathoufresh/light-theme)
	(load-theme nathoufresh/dark-theme t)
	(set-cursor-color "White")
	(setq default-frame-alist '((cursor-color . "Pink")))
	(add-to-list 'default-frame-alist '(font . "Source Code Pro-11"))
	(add-to-list 'default-frame-alist '(fullscreen . maximized)))
    (file-error (progn
		  (package-refresh-contents)
		  (package-install nathoufresh/dark-theme-package)
		  (message "restart emacs")))))


(defun nathoufresh/set-light-theme ()
  (condition-case nil
      (progn
	(require nathoufresh/light-theme-package)
	(disable-theme nathoufresh/dark-theme)
	(load-theme nathoufresh/light-theme t)
	(set-cursor-color "Black")
	(setq default-frame-alist '())
	(add-to-list 'default-frame-alist (cons 'cursor-color "Black"))
	(add-to-list 'default-frame-alist '(font . "Source Code Pro-11"))
	(add-to-list 'default-frame-alist '(fullscreen . maximized)))
    (file-error (progn
		  (package-refresh-contents)
		  (package-install nathoufresh/light-theme-package)
		  (message "restart emacs")))))

(toggle-theme)

(add-hook 'prog-mode-hook 'display-line-numbers-mode)
(add-hook 'org-mode-hook 'display-line-numbers-mode)

(use-package doom-modeline
  :config
  (setq doom-modeline-icon t)
  (setq doom-modeline-minor-modes nil)
  (setq doom-modeline-color-icons t)
  (setq doom-modeline-buffer-state-icon t)
  (setq doom-modeline-buffer-modification-icon nil)
  (setq doom-modeline-buffer-encoding nil)
  (setq doom-modeline-hud t)
  (setq doom-modeline-lsp nil)
  (setq doom-modeline-env-version t)
  (doom-modeline-mode 1))

;; remove/fix annoying ui elements
(setq inhibit-startup-screen t)
(defun nathoufresh/remove-gui-stuff (frame)
  (select-frame frame)
  (if (display-graphic-p frame)
      (progn
	(menu-bar-mode -1)
	(scroll-bar-mode -1)
	(tool-bar-mode -1))))

(add-hook 'after-make-frame-functions 'nathoufresh/remove-gui-stuff)
(add-hook 'after-init-hook (lambda () (funcall 'nathoufresh/remove-gui-stuff (selected-frame))))

(use-package beacon
  :hook (after-init . beacon-mode))

(setq cursor-in-non-selected-windows nil)
(setq blink-cursor-mode nil)
(setq frame-resize-pixelwise t)
(setq gui-select-enable-clipboard t)
(setq save-interprogram-paste-before-kill t)
(defalias 'yes-or-no-p 'y-or-n-p)

;;Navigation
(defun nathoufresh/jk ()
  (interactive)
  (let* ((initial-key ?j)
         (final-key ?k)
         (timeout 0.3)
         (event (read-event nil nil timeout)))
    (if event
        ;; timeout met
        (if (and (characterp event) (= event final-key))
	    (evil-normal-state)
          (insert initial-key)
          (push event unread-command-events))
      ;; timeout exceeded
      (insert initial-key))))

(use-package undo-tree
  :hook
  (after-init . global-undo-tree-mode))

(setq windmove-wrap-around t)

(defun nathoufresh-evil-window-vsplit ()
  (interactive)
  (call-interactively 'evil-window-vsplit)
  (let ((number-of-windows (length (window-list))))
    (if (eq number-of-windows 2)
	(progn
	  (display-buffer (other-buffer) t)
	  (call-interactively 'other-window)))))

(use-package evil
  :init
  (setq evil-emacs-state-modes nil)
  (setq evil-insert-state-modes nil)
  (setq evil-motion-state-modes nil)
  (setq evil-kill-on-visual-paste nil
	evil-want-minibuffer t
	evil-escape-key-sequence "jk"
	evil-want-Y-yank-to-eol t
	evil-respect-visual-line-mode t
	evil-want-integration t ;; evil-collection
	evil-move-beyond-eol nil
	evil-want-keybinding nil
	evil-undo-system 'undo-tree)
  :bind
  (:map evil-insert-state-map
	("j" . nathoufresh/jk))
  (:map evil-normal-state-map
	("F" . avy-goto-char-timer))
  (:map evil-visual-state-map
	("F" . avy-goto-char-timer))
  (:map evil-motion-state-map
	("C-w v" . nathoufresh-evil-window-vsplit))
  :config
  (evil-set-initial-state 'eaf-mode 'emacs)
  :hook
  (after-init . evil-mode))

(use-package evil-collection
  :config
  (setq evil-collection-calendar-want-org-bindings t)
  :hook
  (profiler-report-mode . (lambda () (evil-collection-init 'profiler))))

(use-package evil-surround
  :config
  (global-evil-surround-mode 1))

(global-set-key (kbd "C-x k") 'kill-current-buffer)

(defun kill-buffer-and-delete-window ()
  (interactive)
  (kill-current-buffer)
  (delete-window))

(global-set-key (kbd "C-c k") 'kill-buffer-and-delete-window)

(use-package buffer-move
  :bind
  ("C-x <down>" . buf-move-down)
  ("C-x <up>" . buf-move-up)
  ("C-x <right>" . buf-move-right)
  ("C-x <left>" . buf-move-left))

(use-package counsel)
(use-package swiper)

(use-package ivy
  :init
  (setq ivy-use-selectable-prompt t)
  (setq ivy-use-virtual-buffers t)
  (setq enable-recursive-minibuffers t)
  (setq counsel-yank-pop-after-point t)
  (setq yank-pop-change-selection t)
  :bind
  ("M-x" . counsel-M-x)
  ("C-M-x" . counsel-compile)
  ;; ("C-s" . swiper-thing-at-point-region-only)
  ("C-s" . swiper)
  ("M-p" . counsel-yank-pop)
  ("C-x C-f" . counsel-find-file)
  ("C-x b" . counsel-switch-buffer)
  (:map ivy-minibuffer-map
	("C-h" . ivy-previous-history-element)
	("C-l" . ivy-next-history-element)
	("C-k" . ivy-previous-line)
	([remap evil-insert-digraph] . ivy-previous-line) ;;C-k
	([remap ivy-switch-buffer-kill] . ivy-previous-line) ;; C-k
	("C-j" . ivy-next-line)
	([remap evil-ret] . ivy-done)
	([remap ivy-switch-buffer] . ivy-next-line)
	([remap evil-paste-after] . yank)) ;; C-j
  (:map swiper-map
	([remap swiper-recenter-top-bottom] . ivy-next-history-element) ;; C-l
	([remap evil-ret] . ivy-done))
  :hook (after-init . ivy-mode))

(use-package avy
  :bind ("C-:" . avy-goto-char))

(use-package which-key
  :hook
  (after-init . which-key-mode))


;;programming
(use-package treemacs
  :config (setq treemacs-RET-actions-config
 		'((root-node-open . treemacs-toggle-node)
 		  (root-node-closed . treemacs-toggle-node)
 		  (dir-node-open . treemacs-toggle-node)
 		  (dir-node-closed . treemacs-toggle-node)
 		  (file-node-open . treemacs-visit-node-in-most-recently-used-window)
 		  (file-node-closed . treemacs-visit-node-in-most-recently-used-window)
 		  (tag-node-open . treemacs-toggle-node-prefer-tag-visit)
 		  (tag-node-closed . treemacs-toggle-node-prefer-tag-visit)
 		  (tag-node . treemacs-visit-node-default)))
  :hook (treemacs . treemacs-tag-follow-mode))

;; treemacs evil
(use-package evil
  :config
  (evil-define-key 'normal treemacs-mode-map
    (kbd "cf") 'treemacs-create-file)
  (evil-define-key 'normal treemacs-mode-map
    (kbd "cd") 'treemacs-create-dir)
  (evil-define-key 'normal treemacs-mode-map
    (kbd "R") 'treemacs-rename)
  (evil-define-key 'normal treemacs-mode-map
    (kbd "d") 'treemacs-delete-file)
  (evil-define-key 'normal treemacs-mode-map
    (kbd "K") 'treemacs-goto-parent-node))

(show-paren-mode 1)
(setq show-paren-delay -1)
(add-hook 'prog-mode-hook 'electric-pair-local-mode)

(setq show-trailing-whitespace t)

(use-package highlight-indent-guides
  :init
  (defun highlight-indent-guides-auto-set-faces-with-frame (frame)
    (with-selected-frame frame
      (highlight-indent-guides-auto-set-faces)))
  (if (daemonp)
      (add-hook 'after-make-frame-functions #'highlight-indent-guides-auto-set-faces-with-frame))
  (setq highlight-indent-guides-method 'character)
  :config
  (setq highlight-indent-guides-delay 0)
  (setq highlight-indent-guides-responsive 'stack)
  :hook (python-mode . highlight-indent-guides-mode)
  (web-mode . highlight-indent-guides-mode))

;;Indent whole buffer
(defun indent-buffer ()
  (interactive)
  (if (not (string= major-mode 'python-mode))
      (indent-region (point-min) (point-max))
    (message "Not in python you silly...")))
(global-set-key (kbd "M-i") 'indent-buffer)

(use-package quickrun
  :config (setq quickrun-timeout-seconds -1)
  (setq quickrun-focus-p nil)
  :bind
  ("C-x q" . quickrun)
  ("C-x s" . quickrun-shell))

(use-package magit
  :init (evil-collection-init 'magit)
  :bind
  ("C-x g" . magit-status))

(defun comment-uncomment-on-line-or-region ()
  "Comment or uncomment current line or region if one is selected"
  (interactive)
  (let (beg end)
    (if (region-active-p)
	(setq beg (region-beginning) end (region-end))
      (setq beg (line-beginning-position) end (line-end-position)))
    (comment-or-uncomment-region beg end)
    (next-line)))
(global-set-key (kbd "M-c") 'comment-uncomment-on-line-or-region)

(use-package smart-hungry-delete
  :bind
  ("<backspace>" . smart-hungry-delete-backward-char)
  ("C-d" . smart-hungry-delete-forward-char))

(use-package yasnippet
  :init
  :bind
  (:map yas-minor-mode-map
	("C-l" . yas-next-field)
	("C-h" . yas-prev-field))
  :hook (prog-mode . yas-minor-mode))

(use-package company
  :config
  ;; (delete 'company-dabbrev company-backends)
  (setq company-minimum-prefix-length 1)
  (setq company-global-modes '(not eshell-mode org-mode))
  (setq company-idle-delay 0.5)
  :bind
  (:map company-search-map
	("C-k" . company-select-previous)
	("C-j" . company-select-next))
  (:map company-active-map
	("C-k" . company-select-previous)
	("C-j" . company-select-next))
  :hook
  (after-init . global-company-mode))

(use-package web-mode
  :init
  (setq web-mode-enable-auto-pairing t)
  (setq web-mode-enable-current-element-highlight t)
  (add-to-list 'auto-mode-alist '("\\.php\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.html\\'" . web-mode))
  :config
  (setq indent-tabs-mode t))

(use-package dap-mode
  :init
  (require 'dap-python))

;;org mode
(use-package org-bullets
  :config
  (setq org-hide-leading-stars t)
  (setq org-ellipsis "⤵")
  (setq org-bullets-face-name (quote org-bullet-face))
  (setq org-bullets-bullet-list '("☉" "◉" "○" "◎" "○" "◌"))
  :hook (org-mode . (lambda () (org-bullets-mode 1))))

(use-package org
  :config
  (custom-set-variables
   '(org-directory "~/Notes")
   '(org-agenda-files (list org-directory))))


(use-package flyspell
  :ensure nil
  :config
  (setq flyspell-default-dictionary "french"))

(use-package flyspell-correct
  :after flyspell
  :bind (:map flyspell-mode-map ("C-;" . flyspell-correct-wrapper)))

(use-package flyspell-correct-ivy
  :after flyspell-correct)

(use-package evil
  :config
  (evil-define-key 'normal org-agenda-mode-map
    (kbd "j") 'org-agenda-next-line)
  (evil-define-key 'normal org-agenda-mode-map
    (kbd "k") 'org-agenda-previous-line)
  (evil-define-key 'normal org-agenda-mode-map
    (kbd "l") 'right-char)
  (evil-define-key 'normal org-agenda-mode-map
    (kbd "h") 'left-char)
  (evil-define-key 'normal org-agenda-mode-map
    (kbd "<RET>") 'org-agenda-switch-to)
  (evil-define-key 'normal org-mode-map
    (kbd "K") 'outline-previous-visible-heading)
  (evil-define-key 'normal org-mode-map
    (kbd "<tab>") 'org-cycle)
  (evil-define-key 'normal org-mode-map
    (kbd "J") 'outline-next-visible-heading))

(use-package arc-mode
  :init (evil-collection-init 'arc-mode))

(use-package perfect-margin
  :config
  (setq perfect-margin-ignore-regexps
	'(".*[^org]$"))
  (setq perfect-margin-visible-width 100))

(defun nathoufresh/bracket ()
  (interactive)
  (let* ((initial-key ?{)
	 (final-key ?{)
	 (timeout 0.3)
	 (event (read-event nil nil timeout)))
    (if event
	;; timeout met
	(if (and (characterp event) (= event final-key))
	    (progn
	      (insert "{}{}")
	      (backward-char 3))
	  (progn
	    (insert "{}")
	    (backward-char 1)
	    (push event unread-command-events)))
      ;; timeout exceeded
      (progn
	(insert "{}")
	(backward-char 1)))))

(setq org-default-header "
#+OPTIONS: \\n:t toc:nil
#+LATEX_HEADER: \\usepackage[margin=2cm]{geometry}
#+LATEX: \\setlength\\parindent{0pt}
#+EXCLUDE_TAGS: noexport")

(defun org-new-doc ()
  (interactive)
  (call-interactively 'find-file)
  (let ((file-name (file-name-base (buffer-file-name))))
    (insert (concat "#+TITLE: " file-name))
    (insert org-default-header)))

(use-package calfw)
(use-package calfw-org)

(use-package org
  :init
  (setq org-show-context-detail '((agenda . local)
				  (bookmark-jump . lineage)
				  (isearch . lineage)
				  (default . canonical)))
  (setq org-plain-list-ordered-item-terminator 41)
  (setq org-agenda-files '("~/Notes/Perso.org" "~/Notes/Scout.org"))

  ;;syntax highlighting
  ;; minted
  (setq org-src-fontify-natively t)
  (add-to-list 'org-latex-packages-alist '("" "minted"))
  (setq org-latex-listings 'minted)
  (setq org-latex-minted-options
	'(("breaklines=true") ("linenos=true")))
  (setq org-latex-pdf-process
	'("pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"
	  "pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"
	  "pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"))
  ;; engrave-faces
  ;; (use-package engrave-faces
  ;; :load-path "~/.emacs.d/engrave-faces"
  ;; :config
  ;; (setq org-latex-listings 'engrave))
  
  (setq org-file-apps '(("\\.pdf\\'" . "okular %s")))
  (setq org-agenda-start-on-weekday 1)
  (setq calendar-week-start-day 1)
  (setq org-image-actual-width nil)
  (defun org-font-size () (text-scale-increase 5))
  :bind (("\C-cl" . org-store-link)
	 ("\C-ca" . org-agenda)
	 ("\C-cc" . org-capture)
	 ("\C-cb" . org-switchb)
	 ("{" . nathoufresh/bracket))
  :custom
  (org-todo-keywords (quote ((sequence "TODO" "WAITING" "DONE"))))
  (org-format-latex-options
   (quote (:foreground default :background default :scale 1.4
		       :html-foreground "Black" :html-background "Transparent" :html-scale 1.0 :matchers
		       ("begin" "$1" "$" "$$" "\\(" "\\["))))
  :config
  (setq org-beamer-frame-level 2)
  ;; (setq org-beamer-theme "metropolis")
  :hook
  (org-mode . org-indent-mode)
  (org-mode . org-font-size))

(use-package htmlize)

(use-package org-mind-map
  :init
  (require 'ox-org)
  (setq org-mind-map-include-text t)
  :config
  (setq org-mind-map-engine "dot"))

(use-package org
  :bind
  ("M-l" . org-toggle-latex-fragment)
  :hook
  (org-mode . visual-line-mode))

(use-package org-preview-latex-equation
  :load-path "org-preview-latex-equation"
  :hook (org-mode . org-preview-latex-equation-mode))

(use-package org-reddit
  :load-path "reddit"
  :bind ("C-x x" . org-reddit))

(defun org-clock-report-hours-duration ()
  (interactive)
  (let ((org-duration-format 'h:mm))
    (call-interactively 'org-clock-report)))

;; create flashcards from org mode file
(setq org-memo-beamer-options-string "#+OPTIONS: toc:nil title:nil author:nil prop:nil \\n:t\n#+LATEX_HEADER: \\usepackage{ocgx}\n")

(defun org-memo-create-flashcards-file ()
  (interactive)
  (funcall 'org-memo-create-flashcards 'file))

(defun org-memo-create-flashcards-tree ()
  (interactive)
  (funcall 'org-memo-create-flashcards 'tree))

(defun org-memo-create-flashcards (SCOPE)
  (setq org-memo-entry-counter 0)
  (with-current-buffer (get-buffer-create "*memo.org*")
    (org-mode)
    (erase-buffer)
    (insert (concat org-memo-beamer-options-string "\n")))
  (org-map-entries 'org-memo-copy-entries "QUESTION<>\"\"" SCOPE)
  (with-current-buffer (get-buffer-create "*memo.org*")
    (if (> org-memo-entry-counter 0)
	(let ((org-beamer-frame-level 1))
	  (org-beamer-export-to-pdf))
      (message "No questions found..."))))

(defun org-memo-copy-entries ()
  (setq org-memo-entry-counter (+ org-memo-entry-counter 1))
  (org-mark-subtree)
  (let ((buf (current-buffer))
	(beg (region-beginning))
	(end (region-end))
	(question (org-entry-get (point) "QUESTION")))
    (deactivate-mark)
    (with-current-buffer (get-buffer-create "*memo.org*")
      (goto-char (point-max))
      (insert-buffer-substring buf beg end)
      (outline-previous-heading)
      (kill-whole-line)
      (insert (concat "* " question "\n"))
      (org-delete-property "QUESTION")
      (insert (concat "#+latex: \\begin{ocg}{Q" (number-to-string org-memo-entry-counter) "}{ocg" (number-to-string org-memo-entry-counter)  "}{0}"))
      (org-mark-subtree)
      (goto-char (region-end))
      (deactivate-mark)
      (insert (concat "#+latex: \\end{ocg}\\begin{center}\\switchocg{ocg" (number-to-string org-memo-entry-counter) "}{Réponse}\\end{center}\n")))))

(defun org-memo-add-question ()
  (interactive)
  (org-set-property "QUESTION" "")
  (next-line)
  (org-cycle)
  (next-line)
  (evil-append-line 1)
  (insert " "))

(use-package org
  :bind ([remap org-set-tags-command] . org-memo-add-question))
;; end flashcards

;;miscellaneous

(use-package all-the-icons-dired)
(use-package dired
  :ensure nil
  :init (evil-collection-init 'dired)
  :config (setq dired-listing-switches "-alh")
  :hook
  (dired-mode . all-the-icons-dired-mode))

(use-package auto-sudoedit
  :hook (after-init . auto-sudoedit-mode))

(setq truncates-line nil)

(setq backup-inhibited t)

;; auto save buffer
(setq auto-save-default nil)

(if (not (boundp 'nathoufresh/autosave-running))
    (defvar nathoufresh/autosave-running nil
      "Contains the autosave timer"))
(if (not (boundp 'nathoufresh/autosave-buffer-list))
    (defvar nathoufresh/autosave-buffer-list nil
      "List containing the buffers to save at interval described in nathoufresh/autosave-interval"))
(defvar nathoufresh/autosave-interval "30 sec"
  "Interval for autosaving the buffers specified in nathoufresh/autosave-buffer-list")
(setq nathoufresh/autosave-interval "5 sec")

(defun nathoufresh/save-buffer ()
  (let ((inhibit-message t))
    (dolist (buffer nathoufresh/autosave-buffer-list)
      (with-current-buffer buffer
	(progn
	  (if (buffer-modified-p)
	      (if (buffer-file-name)
		  (if (file-exists-p (file-name-directory (buffer-file-name)))
		      (save-buffer)))))))
    (if (not (= (length nathoufresh/autosave-buffer-list) 0))
	(setq nathoufresh/autosave-running (run-at-time nathoufresh/autosave-interval nil 'nathoufresh/save-buffer))
      (setq nathoufresh/autosave-running nil))))

(defun nathoufresh/remove-from-autosave-list ()
  (if (member (current-buffer) nathoufresh/autosave-buffer-list)
      (progn
	(setq nathoufresh/autosave-buffer-list (delete (current-buffer) nathoufresh/autosave-buffer-list))
	(if (= (length nathoufresh/autosave-buffer-list) 0)
	    (progn
	      (cancel-timer nathoufresh/autosave-running)
	      (setq nathoufresh/autosave-running nil))))))

(define-minor-mode nathoufresh/autosave-mode
  nil nil nil nil
  (if nathoufresh/autosave-mode
      (progn
	(add-hook 'kill-buffer-hook 'nathoufresh/remove-from-autosave-list)
	(add-to-list 'nathoufresh/autosave-buffer-list (current-buffer))
	(if (not nathoufresh/autosave-running)
	    (progn
	      (funcall 'nathoufresh/save-buffer))))
    (progn
      (remove-hook 'kill-buffer-hook 'nathoufresh/remove-from-autosave-list)
      (funcall 'nathoufresh/remove-from-autosave-list))))

(add-hook 'prog-mode-hook 'nathoufresh/autosave-mode)
(add-hook 'org-mode-hook 'nathoufresh/autosave-mode)
;; end autosave

(use-package visual-fill-column
  :init
  (setq visual-fill-column-center-text t)
  (setq visual-fill-column-width 150))

;; epub reader
(use-package nov
  :init (evil-collection-init 'nov)
  :config (setq nov-text-width 100)
  :hook ((nov-mode . hl-line-mode)
	 (nov-mode . visual-line-mode)
	 (nov-mode . visual-fill-column-mode))
  :bind
  ([remap describe-mode] . evil-backward-char)
  ([remap nov-history-back] . evil-forward-char))
(add-to-list 'auto-mode-alist '("\\.epub\\'" . nov-mode))

(use-package eshell
  :init (defun eshell/ff (&rest args)
	  (while args (if (string-match "\\`\\+\\([0-9]+\\)\\'" (car args))
			  (let* ((line (string-to-number (match-string 1 (pop args)))) (file (pop args))) (find-file file) (goto-line line)) (find-file (pop args)))))
  (defun eshell/cl () (interactive) (let ((inhibit-read-only t)) (erase-buffer) (eshell-send-input))))

(add-hook 'after-init-hook '(lambda () (savehist-mode 1)))

(setq text-scale-mode-step 1.05)

(setq scroll-conservatively 100)

(use-package good-scroll
  :config
  (good-scroll-mode 1))

(use-package expand-region
  :bind
  ("M-SPC" . er/expand-region))

;;Mail, sensible
(let ((mailconfig "~/.emacs.d/mail.el"))
  (if (file-exists-p mailconfig)
      (load mailconfig)))
;;RSS
(use-package elfeed
  :init (evil-collection-init 'elfeed)
  :config
  (setq-default elfeed-search-filter "@3-week-ago +unread +Linux")
  (setq elfeed-feeds
	'(("https://lineageos.org/feed.xml" Android)
	  ("https://f-droid.org/feed.xml" Android)
	  ("https://www.androidhive.info/feed/" Android)
	  ("http://rss.rtbf.be/media/rss/audio/lapremiere_recent.xml" Info)
	  ("https://feeds.feedburner.com/Rtlinfos-ALaUne" Info)
	  ("https://www.20minutes.fr/feeds/rss-monde.xml" Info)
	  ("https://www.phoronix.com/rss.php" Linux)
	  ("https://latenightlinux.com/feed/mp3" Linux)
	  ("https://forum.manjaro.org/c/announcements.rss" Linux)
	  ("https://feeds.feedburner.com/RTLSports" Sport)
	  ("https://www.theroar.com.au/rugby-union/feed/" Sport)
	  ("https://www.wired.com/feed/rss" Tech)
	  ("https://www.technologyreview.com/feed/" Tech)
	  ("https://www.technologyreview.com/topic/computing/feed/" Tech)))
  :bind ("C-x w" . elfeed))

;; hexl mode pour ouvrir des fichier binaire
(add-to-list 'auto-mode-alist '("\\.dat\\'" . hexl-mode))

;; open pdf externally
(use-package openwith
  :init
  ;; make outbonding attachments in mail working
  (add-to-list  'mm-inhibit-file-name-handlers 'openwith-file-handler)
  (setq openwith-associations '(("\\.pdf\\'" "okular" (file)))))

(openwith-mode 1)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(org-agenda-files '("/home/nathan/Notes/Perso.org"))
 '(org-directory "~/Notes")
 '(package-selected-packages
   '(good-scroll openwith open-with csv-mode org-clock-csv engrave-faces kaolin-themes use-package lsp-mode))
 '(warning-suppress-log-types '((comp))))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(mu4e-highlight-face ((t nil))))
(put 'dired-find-alternate-file 'disabled nil)
