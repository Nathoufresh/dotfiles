#!/bin/sh

server=emacs
client=emacsclient
server_cmd="$server --bg-daemon"
client_cmd="$client -cqu"

pgrep -x $server > /dev/null && server_running=1 || server_running=0
pgrep -x $client > /dev/null && client_running=1 || client_running=0

if [ $server_running -eq 0 ]; then
	eval "$server_cmd"
fi

if [ $client_running -eq 0 ]; then
	eval "$client_cmd"
fi
