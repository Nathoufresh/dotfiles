:imap jk <Esc>
syntax on
set nu
set ai
set autoindent
set clipboard=unnamedplus
let &t_SI = "\e[6 q"
let &t_EI = "\e[0 q"
